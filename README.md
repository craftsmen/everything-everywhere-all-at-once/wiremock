# wiremock

## Description
This projects sets up a mock server that can be used in the "Everything Everywhere All at Once" workshop.

## Installation
To build the docker image, use this command: 
```docker
docker build -t wiremock .
```

## Usage
To run the docker image, use this command: 
```docker
docker run -dp 8080:8080 wiremock
```

## Clean-up
To stop the docker image, use this command: 
```docker
docker stop $(docker ps -a -q --filter ancestor=wiremock --format="{{.ID}}")
```
To remove the docker image, use this command: 
```docker
docker rm $(docker ps -a -q --filter ancestor=wiremock --format="{{.ID}}")
```
