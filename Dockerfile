FROM wiremock/wiremock

EXPOSE 8080
CMD ["--async-response-enabled=true", "--no-request-journal", "--disable-request-logging"]
COPY mappings /home/wiremock/mappings